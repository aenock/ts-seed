import { createModels } from './models/index';
import * as express from 'express';
import * as winston from 'winston';
import * as dotenv from 'dotenv';
import * as cors from 'cors';
import * as useragent from 'express-useragent';
import 'express-async-errors';

import startRoutes from './routes';
import startLogs from './controllers/logging';

dotenv.config({ path: __dirname + '/../.env' });
const dbConfigs = require('../configs/config.json');
const app: express.Application = express();
app.use(cors());
app.use(express.json());
app.use(useragent.express());

export const consoleLogger = winston.createLogger({ transports: [ new winston.transports.Console({ format: winston.format.simple() }) ] });
export const apiLogger = winston.createLogger({ transports: [ new winston.transports.Console({ format: winston.format.simple() }), new winston.transports.File({filename: __dirname + '/../logs/apiLogs.log'}) ] });
export const uncaughtLogger = winston.createLogger({ transports: [ new winston.transports.Console({ format: winston.format.simple() }), new winston.transports.File({filename: __dirname + '/../logs/uncaughtLogs.log'}) ] });
export const unhandledLogger = winston.createLogger({ transports: [ new winston.transports.Console({ format: winston.format.simple() }), new winston.transports.File({filename: __dirname + '/../logs/unhandledLogs.log'}) ] });

startLogs();
export const db = createModels(dbConfigs[process.env.NODE_ENV]);
db.sequelize.sync(/*{ force: true }*/).then(function() {consoleLogger.info('Nice! Database looks fine')}).then(() => {});

startRoutes(app);

const port = process.env.PORT;
app.listen(port, () => consoleLogger.info(`Server listening to port ${port}`));