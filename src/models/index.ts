import { EmployeeFactory } from './Employee';
import * as Sequelize from 'sequelize';
import { DbInterface } from '../typings/DbInterface';

interface DatabaseParams{
    dialect: string;
    host: string;
    operatorsAliases: boolean;
    logging: boolean;
}

interface DatabaseConfigs {
    username: string;
    password: string;
    database: string;
    params: DatabaseParams;
}

export const createModels = (dbConfigs: DatabaseConfigs): DbInterface => {
    const { database, username, password, params } = dbConfigs;
    const sequelize = new Sequelize(database, username, password, params);

    const db: DbInterface = {
        sequelize,
        Sequelize,
        Employee: EmployeeFactory(sequelize, Sequelize)
    }

    Object.keys(db).forEach(modelName => {
        if (db[modelName].associate) {
            db[modelName].associate(db);
        }
    });

    return db;
}