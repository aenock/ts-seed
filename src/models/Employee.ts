import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from '../typings/SequelizeAttributes';


export interface EmployeeAttributes{
    id?: number;
    date_of_birth: string;
    email: string;
    employee_number: string;
    first_name: string;
    gender: string;
    id_number: string;
    last_name: string;
    middle_name: string;
    phone: string;
    place_of_birth: string;
    residence: string;
    shared: boolean;
    status: number;
    department: number;
    department_group: number;
    factory: number;
    factory_group: number;
    next_of_kin: number;
    nhif: number;
    position: number;
    position_group: number;
    section: number;
    section_group: number;
    is_day_shift: boolean;
    last_attendance: Date;
}

export interface EmployeeInstance extends Sequelize.Instance<EmployeeAttributes>, EmployeeAttributes{}

export const EmployeeFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<EmployeeInstance, EmployeeAttributes> => {
    const attributes: SequelizeAttributes<EmployeeAttributes> = {
        date_of_birth: { type: DataTypes.STRING },
        email: { type: DataTypes.STRING },
        employee_number: { type: DataTypes.STRING },
        first_name: { type: DataTypes.STRING },
        gender: { type: DataTypes.STRING },
        id_number: { type: DataTypes.STRING },
        last_name: { type: DataTypes.STRING },
        middle_name: { type: DataTypes.STRING },
        phone: { type: DataTypes.STRING },
        place_of_birth: { type: DataTypes.STRING },
        residence: { type: DataTypes.STRING },
        shared: { type: DataTypes.BOOLEAN },
        status: { type: DataTypes.INTEGER },
        department: { type: DataTypes.BIGINT },
        department_group: { type: DataTypes.BIGINT },
        factory: { type: DataTypes.BIGINT },
        factory_group: { type: DataTypes.BIGINT },
        next_of_kin: { type: DataTypes.BIGINT },
        nhif: { type: DataTypes.BIGINT },
        position: { type: DataTypes.BIGINT },
        position_group: { type: DataTypes.BIGINT },
        section: { type: DataTypes.BIGINT },
        section_group: { type: DataTypes.BIGINT },
        is_day_shift: { type: DataTypes.BOOLEAN },
        last_attendance: { type: DataTypes.DATE }
    }

    const Employee = sequelize.define<EmployeeInstance, EmployeeAttributes>('employee', attributes, { timestamps: false });

    Employee.associate = (models) => {
    };

    return Employee;
}