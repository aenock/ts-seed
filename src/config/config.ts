export const errorResponses = (status, description, error) => {
    return {
        status: status,
        description: description,
        error: error
    }
}