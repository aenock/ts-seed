import { apiErrorLogger } from '../server'

import { errorResponses } from '../config/config'

export default function(err, req, res, next){
    apiErrorLogger.error(err.message);
    res.status(500).send(errorResponses('01','Internal server error',['Internal server error']));
}
