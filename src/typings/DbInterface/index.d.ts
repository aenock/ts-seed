import { EmployeeAttributes } from './../../models/Employee';
import * as Sequelize from "sequelize";
import { EmployeeInstance } from "../../models/Employee";

export interface DbInterface{
    sequelize: Sequelize.Sequelize;
    Sequelize: Sequelize.SequelizeStatic;
    Employee: Sequelize.Model<EmployeeInstance, EmployeeAttributes>;
}