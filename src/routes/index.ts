import * as express from 'express';
import error from '../middlewares/error';
import { db } from '../server';
import Datatable from '../controllers/dttable';
const datatable = require(`sequelize-datatable`);

export default function(app: express.Application){

    app.get('/', async (req, res) => {

        console.log(req.connection.remoteAddress);
        

        res.send(req.useragent);
    });

    app.post('/employees', async (req, res) => {
        const reqdata = req.body;
        
        datatable(db.Employee, reqdata, {
            attributes: ['id', 'date_of_birth', 'email', 'first_name', 'gender']
        }).then((result) => {
            return res.send(result);
        }).catch((err) => {
            return res.send({ message: 'Too bad nigga' });
        });
        
    });

    app.get('/datatable', async(req, res) => {
        const attrs = ['id', 'employeeNumber', 'firstName', 'lastName', 'idNumber', 'phone', 'email', 'factory', 'department', 'section', 'position', 'date'];
        const length = 10;
        const url = '';
        const draw = 1;
        const dttableObj = new Datatable(attrs, length, url);
        res.send({ mycols: dttableObj.columns });
    });

    app.use(error);

}