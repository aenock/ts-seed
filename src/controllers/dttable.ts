
interface SearchInterface{
    value: string;
    regex: boolean;
}

interface ColumnInterface{
    data: string;
    name: string;
    searchable: boolean;
    orderable: boolean;
    search: SearchInterface
}

interface OrderInterface{
    column: number,
    dir: 'asc' | 'desc'
}

interface DataTableInterface{
    draw: number;
    columns: Array<ColumnInterface>;
    order: OrderInterface;
    start: number;
    length: number;
    search: SearchInterface;
}


export default class Datatable{

    public draw: number = 1;
    public columns: Array<ColumnInterface>;
    public order: OrderInterface = {
        column: 0,
        dir: 'asc'
    };
    public start: number = 0;
    public length: number;
    public search: SearchInterface = {
        value: '',
        regex: false
    };
    public recordsTotal: number;
    public recordsFiltered: number; 

    constructor(column: Array<string>, len: number, url: string){
        this.columns = this.buildColumnsAray(column);
    }

    buildColumnsAray(colArray: Array<string>): Array<ColumnInterface>{
        return colArray.map(function(col){
            return {
                data: col,
                name: "",
                searchable: true,
                orderable: true,
                search: {
                    value: "",
                    regex: false
                }
            }
        });
    }

    fetchData(){
        const body: DataTableInterface = {
            draw: this.draw,
            columns: this.columns,
            order: this.order,
            start: this.start,
            length: this.length,
            search: this.search
        }

        // Send request
        
    }


} 